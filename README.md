# Overview
The [s2i](<https://docs.openshift.com/enterprise/3.0/creating_images/s2i.html>) source for the [ActiveMQ](<https://en.wikipedia.org/wiki/Apache_ActiveMQ>) image and has a dependency on https://bitbucket.org/leonjohan3/ubuntu-java-generic-image. See https://bitbucket.org/leonjohan3/profile/wiki/ActiveMQ%20Docker%20and%20s2i for usage.

# Notes
1.  Set the required ActiveMQ version to use in the .s2i/environment file.
2.  The default installation ActiveMQ credentials have been left unchanged.
3.  To build and deploy the Docker image, run `make deploy`.
