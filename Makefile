THIS_IMAGE_NAME = activemq
DOCKER_IMAGE_NAME = ubuntu-java-generic
#DOCKER_IMAGE_NAME = leonjohan3atyahoodotcom/ubuntu-java-generic
include ./.s2i/environment

.PHONY: build
build:
	git diff --quiet
	test -z "$$(git status --porcelain)" || exit 1
	s2i build . $(DOCKER_IMAGE_NAME) $(THIS_IMAGE_NAME) --incremental=true

.PHONY: deploy
deploy: build
	docker run --restart always -d -u default --name activemq -p 61616:61616 -p 8161:8161 -v $(HOME)/data:/opt/app-root/src/apache-activemq-$(ACTIVE_MQ_VERSION)/data $(THIS_IMAGE_NAME)
